var WebSocketServer = new require('./node_modules/ws');

// подключенные клиенты
var clients = {};
// цвета кнопок клиентов
var colors = {};
// идет ли на данный момент игра
var isGame = false;
// номер игрока, у которого сейчас ход
var curTurn = 0;
// ячейки в таблице, на которые кликнул каждый клиент
var filledCells = {};
// очередность хода клиентов
var order = [];
// границы таблицы, которые сейчас видит клиент
var clientsBoundary = {};

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function createElem(elem, id, parent, before) {
  if (before == undefined) {
      before = null;
  }
  return "n('" + elem + "', '" + id + "', '" + parent + "', '" + before + "');";
}

function deleteElem(elem, id, parent) {
  return "d('" + elem + "', '" + id + "', '" + parent + "');";
}

function setStyleValue(id, key, value) {
  return "ss('" + id + "', '" + key + "', '" + value + "');";
}

function setText(id, value) {
  return "st('" + id + "', '" + value + "');";;
}

function addEventListener(id, name) {
  return "ael('" + id + "', '" + name + "');";;
}

function addRow(ws, i, clientBoundary, before) {
  ws.send(createElem('div', 'r'+i, 'main', before));
  ws.send(setStyleValue('r'+i, "position", "relative"));
  ws.send(setStyleValue('r'+i, "text-align", "center"));
  ws.send(setStyleValue('r'+i, "padding", 0));

  let left = (clientBoundary === undefined) ? 0 : clientBoundary['left'];
  let right = (clientBoundary === undefined) ? 9 : clientBoundary['right'];
  for (var j = left; j <= right; ++j) {
    ws.send(createElem('button', 'c_'+i+'_'+j, 'r'+i));
    ws.send(setStyleValue('c_'+i+'_'+j, "width", "3em"));
    ws.send(setStyleValue('c_'+i+'_'+j, "height", "3em"));
    ws.send(setStyleValue('c_'+i+'_'+j, "outline", 0));
    ws.send(addEventListener('c_'+i+'_'+j, "click"));
  }
}

function delRow(ws, i) {
  ws.send(deleteElem('r'+i, 'main'));
}

function addColumn(ws, i, clientBoundary, before) {
  let top = clientBoundary['top'];
  let bottom = clientBoundary['bottom'];
  if (before === undefined) {
    for (var j = top; j <= bottom; ++j) {
      ws.send(createElem('button', 'c_'+j+'_'+i, 'r'+j));
      ws.send(setStyleValue('c_'+j+'_'+i, "width", "3em"));
      ws.send(setStyleValue('c_'+j+'_'+i, "height", "3em"));
      ws.send(setStyleValue('c_'+j+'_'+i, "outline", 0));
      ws.send(addEventListener('c_'+j+'_'+i, "click"));
    }
  } else {
    for (var j = top; j <= bottom; ++j) {
      ws.send(createElem('button', 'c_'+j+'_'+i, 'r'+j, 'c_'+j+'_'+before));
      ws.send(setStyleValue('c_'+j+'_'+i, "width", "3em"));
      ws.send(setStyleValue('c_'+j+'_'+i, "height", "3em"));
      ws.send(setStyleValue('c_'+j+'_'+i, "outline", 0));
      ws.send(addEventListener('c_'+j+'_'+i, "click"));
    }
  }
}

function delColumn(ws, i, clientBoundary, before) {
  let top = clientBoundary['top'];
  let bottom = clientBoundary['bottom'];

  for (var j = top; j <= bottom; ++j) {
    ws.send(deleteElem('c_'+j+'_'+i, 'r'+j));
  }
}

function sendGame(ws) {
  ws.send(createElem('div', 'main', 0));
  ws.send(setStyleValue('main', 'width', "100%"));
  ws.send(setStyleValue('main', 'height', "100%"));

  for (var i = 0; i < 10; ++i) {
    addRow(ws, i);
  }

  ws.send(createElem('div', 'footer', 0));
  ws.send(setStyleValue('main', 'width', "100%"));
  ws.send(setStyleValue('main', 'height', "100%"));

  ws.send(createElem('div', 'arrows', 'footer'));
  ws.send(setStyleValue('arrows', "position", "relative"));
  ws.send(setStyleValue('arrows', "text-align", "center"));
  ws.send(setStyleValue('arrows', "padding", 0));
  ws.send(setStyleValue('arrows', "margin-top", '1em'));

  ws.send(createElem('button', 'bottom', 'arrows'));
  ws.send(setStyleValue('bottom', "width", "2em"));
  ws.send(setStyleValue('bottom', "height", "2em"));
  ws.send(setStyleValue('bottom', "margin-right", "1em"));
  ws.send(setStyleValue('bottom', "outline", 0));
  ws.send(addEventListener('bottom', "click"));
  ws.send(setText('bottom', "\u2193"));
  ws.send(setStyleValue('bottom', "color", 'red'));

  ws.send(createElem('button', 'top', 'arrows'));
  ws.send(setStyleValue('top', "width", "2em"));
  ws.send(setStyleValue('top', "height", "2em"));
  ws.send(setStyleValue('top', "margin-right", "1em"));
  ws.send(setStyleValue('top', "outline", 0));
  ws.send(addEventListener('top', "click"));
  ws.send(setText('top', "\u2191"));
  ws.send(setStyleValue('top', "color", 'red'));

  ws.send(createElem('button', 'left', 'arrows'));
  ws.send(setStyleValue('left', "width", "2em"));
  ws.send(setStyleValue('left', "height", "2em"));
  ws.send(setStyleValue('left', "margin-right", "1em"));
  ws.send(setStyleValue('left', "outline", 0));
  ws.send(addEventListener('left', "click"));
  ws.send(setText('left', "\u2190"));
  ws.send(setStyleValue('left', "color", 'red'));

  ws.send(createElem('button', 'right', 'arrows'));
  ws.send(setStyleValue('right', "width", "2em"));
  ws.send(setStyleValue('right', "height", "2em"));
  ws.send(setStyleValue('right', "outline", 0));
  ws.send(addEventListener('right', "click"));
  ws.send(setText('right', "\u2192"));
  ws.send(setStyleValue('right', "color", 'red'));

  ws.send(createElem('div', 'turn', 'footer'));
  ws.send(setStyleValue('turn', "position", "relative"));
  ws.send(setStyleValue('turn', "text-align", "center"));
  ws.send(setStyleValue('turn', "padding", 0));
  ws.send(setStyleValue('turn', "margin-top", '1em'));
}

function joinGame(ws) {
  sendGame(ws);
  for (var key in filledCells) {
    for (var j = 0; j < filledCells[key].length; ++j) {
      var msg = setStyleValue(filledCells[key][j], "background-color", colors[key]);;
      ws.send(msg);
    }
  }
}

function checkGame(cell, userCells) {
  var ar =  cell.split('_');
  ar[1] = parseInt(ar[1]);
  ar[2] = parseInt(ar[2]);
  var count = 0;

  for (var i = -4; i <= 4; ++i) {
    if (userCells.indexOf('c_'+(ar[1]+i)+'_'+(ar[2]+i)) != -1) {
      ++count;
      if (count == 5) {
        return true;
      }      
    } else {
      count = 0;
    }
  }

  count = 0;
  for (var i = -4; i <= 4; ++i) {
    if (userCells.indexOf('c_'+(ar[1]-i)+'_'+(ar[2]+i)) != -1) {
      ++count;
      if (count == 5) {
        return true;
      }      
    } else {
      count = 0;
    }
  }

  count = 0;
  for (var i = -4; i <= 4; ++i) {
    if (userCells.indexOf('c_'+(ar[1])+'_'+(ar[2]+i)) != -1) {
      ++count;
      if (count == 5) {
        return true;
      }      
    } else {
      count = 0;
    }
  }

  count = 0;
  for (var i = -4; i <= 4; ++i) {
    if (userCells.indexOf('c_'+(ar[1]+i)+'_'+(ar[2])) != -1) {
      ++count;
      if (count == 5) {
        return true;
      }      
    } else {
      count = 0;
    }
  }

  return false;
}

function paintCells(id, direction) {
  let clientBoundary = clientsBoundary[id];
  for (var key in filledCells) {
    if (direction == 'top' || direction == 'bottom') {
      let r = (direction == 'top') ? clientBoundary['top'] : clientBoundary['bottom'];
      for (let i = clientBoundary['left']; i <= clientBoundary['right']; ++i) {
        let c = 'c_' + r + '_' + i;
        if (filledCells[key].indexOf(c) != -1) {
          let msg = setStyleValue(c, "background-color", colors[key]);
          clients[id].send(msg);
        }
      }
    } else {
      let col = (direction == 'right') ? clientBoundary['right'] : clientBoundary['left'];
      for (let i = clientBoundary['top']; i <= clientBoundary['bottom']; ++i) {
        let c = 'c_' + i + '_' + col;
        if (filledCells[key].indexOf(c) != -1) {
          let msg = setStyleValue(c, "background-color", colors[key]);
          clients[id].send(msg);
        }
      }
    }
  }
}

function shiftTable(id, direction) {
  let clientBoundary = clientsBoundary[id];
  if (direction == 'bottom') {
    delRow(clients[id], clientBoundary['top']);
    ++clientBoundary['top'];
    ++clientBoundary['bottom'];
    addRow(clients[id], clientBoundary['bottom'], clientBoundary);
    paintCells(id, direction);

  } else if (direction == 'top') {
    delRow(clients[id], clientBoundary['bottom']);
    --clientBoundary['bottom'];
    addRow(clients[id], clientBoundary['top']-1, 
           clientBoundary, 'r'+clientBoundary['top']);
    --clientBoundary['top'];
    paintCells(id, direction);

  } else if (direction == 'left') {
    delColumn(clients[id], clientBoundary['right'], clientBoundary);
    --clientBoundary['right'];
    addColumn(clients[id], clientBoundary['left']-1, 
              clientBoundary, clientBoundary['left']);
    --clientBoundary['left'];
    paintCells(id, direction);

  } else if (direction == 'right') {
    delColumn(clients[id], clientBoundary['left'], clientBoundary);
    ++clientBoundary['right'];
    ++clientBoundary['left'];
    addColumn(clients[id], clientBoundary['right'], clientBoundary);
    paintCells(id, direction);
  } 
}

function startGame(){
  clients = {};
  colors = {};
  isGame = false;
  curTurn = 0;
  filledCells = {};
  order = [];
  clientsBoundary = {};
}

var webSocketServer = new WebSocketServer.Server({
  port: 8888
});
webSocketServer.on('connection', function(ws) {

  var id = Math.random();
  clients[id] = ws;
  colors[id] = getRandomColor();
  clientsBoundary[id] = {'left': 0, 'right': 9, 'top': 0, 'bottom': 9};
  order.push(id);
  filledCells[id] = [];
  var endGame = false;

  console.log("Новое соединение " + id);
  if (!isGame) {
    sendGame(ws);
  } else {
    joinGame(ws);
  }

  if (order.indexOf(id) == 0) {
    ws.send(setText('turn', "Ваш ход"));
  } else {
    ws.send(setText('turn', "Ждите свой ход"));
  }

  ws.on('message', function(message) {
    console.log('Получено сообщение от пользователя ' + id + ': ' + message);

    var isNotClickable = false;
    message = JSON.parse(message);
    if (message["eventName"] == 'click') {
      isGame = true;
      if ('topleftrightbottom'.indexOf(message["ptr"]) != -1) {
        shiftTable(id, message["ptr"]);
        isNotClickable = true;
      } else {
        var msg = setStyleValue(message["ptr"], "background-color", colors[id]);
      }
    }

    for (var key in filledCells) {
      if (filledCells[key].indexOf(message['ptr']) != -1) {
        isNotClickable = true;
        break;
      }
    }

    if (order.indexOf(id) == curTurn && !endGame && !isNotClickable) {
      filledCells[id].push(message["ptr"]);
      endGame = checkGame(message["ptr"], filledCells[id]);
      curTurn = endGame ? curTurn : (curTurn+1) % order.length;
      
      for (key in clients) {            
        clients[key].send(msg);

        if (order[curTurn] == key) {
          let cmd = endGame ? 'Вы выиграли. Игра окончена' : 'Ваш ход';
          clients[key].send(setText('turn', cmd));
        } else {
          let cmd = endGame ? 'Вы проиграли. Игра окончена' : 'Ждите свой ход';
          clients[key].send(setText('turn', cmd));
        }

        if (endGame) {
          clients[key].close();
        }
      }
    }
  });

  ws.on('close', function() {
    console.log('Соединение закрыто ' + id);
    delete clients[id];
    let index = order.indexOf(id);
    order.splice(index, 1);
    if (endGame) {
      startGame();
    }
    if (order.length != 0) {
      if (index < curTurn) {
        --curTurn;
      } else if (index == curTurn) {
        curTurn = curTurn % order.length;
        clients[order[curTurn]].send(setText('turn', 'Ваш ход'));
      }
    } else {
      startGame();
    }
  });

});