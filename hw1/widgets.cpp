#include <QApplication>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>
#include <functional>
#include "widgets.h"

extern "C" {

    const char* Object_GetClassName(Object *object) {
        QObject * obj = (QObject*) object;
        QString value = obj->metaObject()->className();
        std::string str = value.toStdString().substr(1);
        char *name = (char*)calloc(str.length()+1, sizeof(char));
        strcpy(name, str.c_str());
        return name;
    }

    void Object_Delete(Object *object) {
        QObject *obj = (QObject*) object;
        delete obj;
    }


    Application* Application_New() {
        char *argv[] = {strdup("Application"), strdup("arg1"), strdup("arg2"),  NULL};
        int argc = (int)(sizeof(argv) / sizeof(char*)) - 1;
        QApplication *app = new QApplication(argc, &argv[0]);
        return (Application*) app;
    }

    int Application_Exec(Application *app) {
        QApplication *appl = (QApplication*) app;
        return appl->exec();
    }


    VBoxLayout* VBoxLayout_New(Widget *parent) {
        QWidget *qparent = (QWidget*) parent;
        QLayout *layout = new QVBoxLayout(qparent);
        return (VBoxLayout*) layout;
    }

    void Layout_AddWidget(Layout *layout, Widget *widget) {
        QWidget *qwidget = (QWidget*) widget;
        QLayout *qlayout = (QLayout*) layout;
        qlayout->addWidget(qwidget);
        layout = (Layout*) qlayout;
    }


    Widget* Widget_New(Widget *parent) {
        QWidget *qparent = (QWidget*) parent;
        QWidget *widget = new QWidget(qparent);
        return (Widget*) widget;
    }

    void Widget_SetVisible(Widget *widget, bool v) {
        QWidget *qwidget = (QWidget*) widget;
        qwidget->setVisible(v);
        widget = (Widget*) qwidget;
    }

    void Widget_SetWindowTitle(struct Widget *widget, const char *title) {
        QWidget *qwidget = (QWidget*) widget;
        std::string str(title);
        QString s = QString::fromStdString(str);
        qwidget->setWindowTitle(s);
        widget = (Widget*) qwidget;
    }

    void Widget_SetLayout(Widget *widget, Layout *layout) {
        QWidget *qwidget = (QWidget*) widget;
        QLayout *qlayout = (QLayout*) layout;
        qwidget->setLayout(qlayout);
        widget = (Widget*) qwidget;
    }

    void Widget_SetSize(Widget *widget, int w, int h) {
        QWidget *qwidget = (QWidget*) widget;
        qwidget->resize(w, h);
        widget = (Widget*) qwidget;
    }


    Label* Label_New(Widget *parent) {
        QWidget *qparent = (QWidget*) parent;
        QLabel *label = new QLabel(qparent);
        return (Label*) label;
    }

    void Label_SetText(Label *label, const char *text) {
        QLabel *qlabel = (QLabel*) label;
        std::string str(text);
        QString s = QString::fromStdString(str);
        qlabel->setText(s);
        label = (Label*) qlabel;
    }


    struct PushButton* PushButton_New(Widget *parent) {
        QWidget *qparent = (QWidget*) parent;
        QPushButton *button = new QPushButton(qparent);
        return (PushButton*) button;
    }

    void PushButton_SetText(PushButton *button, const char *text) {
        QPushButton *qbutton = (QPushButton*) button;
        std::string str(text);
        QString s = QString::fromStdString(str);
        qbutton->setText(s);
        button = (PushButton*) qbutton;
    }

    void PushButton_SetOnClicked(PushButton *button, NoArgumentsCallback *callback) {
        QPushButton *qbutton = (QPushButton*) button;
        Object* sender = (Object*) button;
        QObject::connect(qbutton, &QPushButton::clicked, std::bind(callback, sender));
        button = (PushButton*) qbutton;
    }
    
}