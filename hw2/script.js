var splitFomula = function(formula) {
    var operators = ['(', ')', '+', '-', '*', '/'];
    var new_formula = [];

    var is_operator = false;
    for (var i = 0; i < operators.length; ++i) {
        if (formula.indexOf(operators[i]) != -1) {
            is_operator = true;
            break
        }
    }
    if (!is_operator) {
        return [formula];
    }

    formula = formula.replace(/ /g,'');
    var l = 0;
    for (var i = 0; i < formula.length ; ++i) {
        var token = formula[i];
        if (operators.indexOf(token) != -1) {
            if (l != i) {
                new_formula.push(formula.substring(l, i))
            }
            l = i + 1;
            new_formula.push(token);
        }
    }
    if (l != i) {
        new_formula.push(formula.substring(l, i))
    }
    console.log(new_formula);
    return new_formula;
}

var makeReversePolishNotation = function(formula) {
    var priority = {
        '(': 0,
        ')': 1,
        '+': 2,
        '-': 2,
        '*': 3,
        '/': 3,
        'sin': 3.5,
        'SIN': 3.5,
        'abs': 3.5,
        'ABS': 3.5,
        'len': 3.5,
        'LEN': 3.5
    }
    var new_formula = [];
    var stack = [];

    for (var i = 0; i < formula.length; ++i) {
        var last_token = '(';

        var token = formula[i];
        if (token in priority) {
            if (token == '(') {
                stack.push(token);
            } else if (token == ')') {
                if (!stack.length) {
                    return -1;
                }

                while (stack.length && stack[stack.length - 1] != '(') {
                    new_formula.push(stack.pop());
                }
                stack.pop();
            } else {
                if (token == '-') {
                    if (i == 0 || ((formula[i-1] in priority) && !(formula[i+1] in priority))) {
                        stack.push('_');
                        continue;
                    }
                }

                while (stack.length) {
                    var last_elem = stack[stack.length - 1];
                    if (priority[token] <= priority[last_elem]) {
                        new_formula.push(stack.pop());
                    } else {
                        break;
                    }
                }
                stack.push(token);
            }
        } else {
            new_formula.push(token);
        }

        last_token = token;
    }

    while (stack.length) {
        new_formula.push(stack.pop());
    }
    var count = 0;
    for (var i = 0; i < new_formula.length; ++i) {
        if (new_formula[i] == '(') {
            ++count;
        } else if (new_formula[i] == ')') {
            --count;
        }
    }
    return count ? -1 : new_formula;

}

function isFloat(val) {
    val = val.toString();
    var floatRegex = /^-?\d+(?:[.,]\d*?)?$/;
    if (!floatRegex.test(val))
        return false;

    val = parseFloat(val);
    if (isNaN(val))
        return false;
    return true;
}

var evaluate = function(formula) {
    var operators = {
        '+': (x, y) => x + y,
        '-': (x, y) => x - y,
        '*': (x, y) => x * y,
        '/': (x, y) => (y != 0) ? x / y : '#DIV/0!'
    };

    var functions = {
        'SIN': (x) => Math.sin(x),
        'sin': (x) => Math.sin(x),
        'ABS': (x) => Math.abs(x),
        'abs': (x) => Math.abs(x),
        '_': (x) => -x,
        'LEN' : function(x) {
            var reg = /".*?"/g;

            x = x.toString();
            var match = x.match(reg);
            console.log(match);

            if (match !== null && x.length == match[0].length) {
                return x.length - 2;
            }
            return "#NAME?";
        },
        'len' : function(x) {
            var reg = /".*?"/g;

            x = x.toString();
            var match = x.match(reg);
            console.log(match);

            if (match !== null && x.length == match[0].length) {
                return x.length - 2;
            }
            return "#NAME?";
        }
    };

    var stack = [];
    
    for (var i = 0; i < formula.length; ++i) {
        token = formula[i];

        if (token in operators) {
            if (stack.length == 1) {
                return "#ERROR!";
            } else {
                var y = stack.pop();
                var x = stack.pop();

                if (!isFloat(y) || !isFloat(x)) {
                    return "#ERROR!";
                }
                stack.push(operators[token](x, y));
            }
        } else if (token in functions) {
            if (stack.length == 0) {
                return "#ERROR!";
            }
            var x = stack.pop();
            var returned_value = functions[token](x);
            if (returned_value == '#NAME?') {
                stack = [returned_value];
                break;
            }
            stack.push(returned_value);
        } else {
            if (isFloat(token)) {
                stack.push(parseFloat(token));
            } else {
                stack.push(token);
            }
        }
    }

    if (stack.length == 0 || stack.length > 1) {
        return "#ERROR!";
    }
    return stack.pop();
};

var replaceLinks = function(obj, formula) {
    updateRefs(obj);
    obj.torefs = [];
    for (var i = 0; i < formula.length; ++i) {
        var token = formula[i].toUpperCase();
        if (token.length == 3) {
            var result = token.match(/[A-Z]1[0-9]|[A-Z]20/g);
            if (result !== null && result.length > 0) {
                cell = document.getElementById(result[0]);
                obj.torefs.push(cell.id);
                if (cell.refs.indexOf(obj.id) == -1) {
                    cell.refs.push(obj.id);
                }
                if (!isFloat(cell.value)) {
                    formula[i] = '"' + cell.value + '"';
                } else {
                    formula[i] = cell.value;
                }
            }
        } else if (token.length == 2) {
            var result = token.match(/[A-Z][0-9]/g);
            if (result !== null && result.length > 0) {
                cell = document.getElementById(result[0]);
                obj.torefs.push(cell.id);
                if (cell.refs.indexOf(obj.id) == -1) {
                    cell.refs.push(obj.id);
                }
                if (!isFloat(cell.value)) {
                    formula[i] = '"' + cell.value + '"';
                } else {
                    formula[i] = cell.value;
                }
            }
        }
    }
    return formula;
}

var updateRefs = function(obj) {
    for (var i = 0; i < obj.torefs.length; ++i) {
        var cell = document.getElementById(obj.torefs[i]);
        var index = cell.refs.indexOf(obj.id);
        if (index > -1) {
            cell.refs.splice(index, 1);
        }
    }
}

var computeFormula = function(obj) {
    obj.formula = obj.value;
    var formula = splitFomula(obj.value.substring(1));
    if (formula == '') {
        return;
    }
    formula = replaceLinks(obj, formula);
    var polish_notation = makeReversePolishNotation(formula);
    console.log(polish_notation);
    if (polish_notation == -1) {
        obj.value = '#ERROR!';
    } else {
        obj.value = evaluate(polish_notation);
    }
}

var computeRefs = function(obj) {
    for (var i = 0; i < obj.refs.length; ++i) {
        var id = obj.refs[i];
        var cell = document.getElementById(id);
        cell.value = cell.formula;
        computeFormula(cell);
    }
}

var createCell = function(isWritable, id) {
    var cell = document.createElement('input');
    cell.id = id;
    cell.formula = '';
    cell.refs = [];
    cell.torefs = [];
    if (!isWritable) {
        if (id != 0) {
            cell.value = id;
        }
        cell.readOnly = true;
        cell.disabled = true;
        cell.style.color = "gray";
        cell.style.backgroundColor = "#EBEBE4";
    } else {
        cell.formula = cell.value;
        cell.onfocus = function() {
            this.value = this.formula;
            var column_name = this.id.substring(0,1);
            var column = document.getElementById(column_name);
            column.style.color = "black";
            column.style.backgroundColor = "#d5d0d6";

            var row_name = this.id.substring(1);
            var row = document.getElementById(row_name);
            row.style.color = "black";
            row.style.backgroundColor = "#d5d0d6";

            /*formula = document.getElementById("line");
            formula.value = cell.formula;*/
        }

        cell.onblur = function() {
            var column_name = this.id.substring(0,1);
            var column = document.getElementById(column_name);
            column.style.color = "gray";
            column.style.backgroundColor = "#EBEBE4";

            var row_name = this.id.substring(1);
            var row = document.getElementById(row_name);
            row.style.color = "gray";
            row.style.backgroundColor = "#EBEBE4";

            if (this.value[0] == "=") {
                computeFormula(this);
            }
        }

        cell.onkeypress = function(event) {
            if (!event) {
                event = window.event;
            }
            var x = event.which || event.keyCode; 
            if (x == 13) {
                if (this.value[0] == "=") {
                    this.formula = this.value;
                    computeFormula(this);
                    computeRefs(this);
                } else if (this.formula) {
                    this.value = this.formula;
                }
            }
        };
    }
    return cell;
}

var addHeaders = function(main_table) {
    var charCode = 65;    
    var p = document.createElement('p');
    p.id = "r0";
    for (var i = 0; i <= 26; ++i) {
        var cell = document.createElement('input');
        if (!i) {
            var cell = createCell(false, 0);
            p.appendChild(cell);
            continue;
        }
        var cell = createCell(false, String.fromCharCode(charCode + i - 1));
        p.appendChild(cell);
    }
    main_table.appendChild(p);

    for (var i = 0; i < 20; ++i) {
        var p = document.createElement('p');
        p.id = "r" + (i + 1);
        var cell = createCell(false, i + 1);
        p.appendChild(cell);
        main_table.appendChild(p);
    }
}

var addRow = function(table, main_table) {
    var row = [];
    id = "r" + (table.length + 1);
    var p = document.getElementById(id);
    for (var i = 0; i < table[0].length; ++i) {
        row.push(0);

        cellId = String.fromCharCode(65 + i) + p.id.substring(1);
        var cell = createCell(true, cellId);
        p.appendChild(cell);
    }
    table.push(row);
    main_table.appendChild(p);
}

var addColumn = function(table, main_table) {
    var charCode = 65 + table[0].length;
    for (var i = 0; i < table.length; ++i) {
        table[i].push(0);

        var cellId = String.fromCharCode(charCode + i) + (i + 1);
        var cell = createCell(true, cellId);
        var id = "r" + (i + 1);
        p = document.getElementById(id);
        if (!p) {
            var p = document.createElement('p');
            p.id = "r1";
            main_table.appendChild(p);
        }
        p.appendChild(cell);
    }
}

/*var createFormulaLine = function() {
    var formula = document.getElementById('formula');
    var p = document.createElement('p');
    var sign = createCell(false, "f(x)");
    sign.style.color = "black";
    sign.style.borderRight = "1px solid gray";
    sign.style.backgroundColor = "white";
    p.appendChild(sign);

    var line = document.createElement('input');
    line.id = "line";
    line.style.width = "1272px";
    line.style.textAlign = "left";
    p.appendChild(line);
    formula.appendChild(p);
}*/

var createTable = function(table) {
    var main_table = document.getElementById("main_table");
    addHeaders(main_table);
    for (var i = 0; i < 26; ++i) {
        addColumn(table, main_table);    
    }
    for (var i = 0; i < 19; ++i)
        addRow(table, main_table);
    var save = document.createElement('button');
    save.innerHTML = 'Save table';
    save.onclick = saveData;
    main_table.appendChild(save);
}

var loadData = function() {
    for (var i = 0; i < localStorage.length; ++i)  {
       var key = localStorage.key(i);
       var cell = document.getElementById(key);
       var data = JSON.parse(localStorage.getItem(key));
       cell.value = data[0];
       cell.formula = data[1];
    }
}

var removeData = function() {
    localStorage.clear();
}

var saveData = function() {
    for (var i = 0; i < 20; ++i) {
        for (var j = 0; j < 26; ++j) {
            var id = String.fromCharCode(65 + j) + (i + 1);
            var cell = document.getElementById(String.fromCharCode(65 + j) + (i + 1));
            var data = JSON.stringify([cell.value, cell.formula]);
            localStorage.setItem(cell.id, data);
        }
    }
}

/*var saveDate = functions() {

}*/

window.onload = function()
{
    var table = [[]];

    // createFormulaLine();
    createTable(table);
    // removeData();
    loadData();
}