-module(bmp_image).
-import(lists,[last/1]).
-compile(export_all).
-define(BYTES_PER_PIXEL, 3).
% Limitations:
%  24bpp (R, G, B), no compression

% Functions:
%  load(FileName) -> {ok, Image} | {error, Reason}
%  save(FileName, Image) -> ok | error
%  get_pixel(Image, {X, Y}) -> {R, G, B}
%  change_pixel(Image, {X, Y}, {R, G, B}) -> NewImage
%  get_size(Image) -> {Width, Heigth}

% What is an image??? A struct with fields:
% - width
% - height
% - pixel data contents
% - headers read from file and to be written back
% {Width, Height, Conents, Headers}

% Record is a tuple with named fields
-record(image, {
    width = 0,  % default field value
    height = 0,
    row_size = 0,
    contents,
    headers
}).

load(FileName) -> case file:read_file(FileName) of
    {ok, FileContents}  -> parse_bmp(FileContents);
    SomethingElse        -> SomethingElse  % read error case
    end.

parse_bmp(FileContents) ->
    case FileContents of
        <<"BM",_:64,Off:32/little,_:32,W:32/signed-little,H:32/signed-little,_Rest/binary>> ->
            ContentsSize = size(FileContents) - Off,
            io:fwrite("Image contents offset and size: ~p, ~p~n", [Off, ContentsSize]),
            {ok,
                #image{
                    width=W,
                    height=H,
                    row_size=row_size(W),
                    headers=binary_part(FileContents, 0, Off),
                    contents=binary_part(FileContents, Off, ContentsSize)
                }
            };  % ends match <<"BM",_:64,Off:32/little,....
        
        _  -> {error, wrong_file_format}  % does not match correct header
    end.

save(FileName, Image) ->
    file:write_file(FileName, <<(Image#image.headers)/binary, (Image#image.contents)/binary>>).

get_size(Image) -> {Image#image.width, Image#image.height}.

row_size(W) ->
    BytesPerPixel = ?BYTES_PER_PIXEL,    
    MeaningfullSize = BytesPerPixel * W,
    PaddingSize = case MeaningfullSize rem 4 of
        0               -> 0;
        NotNullPadding  -> 4 - NotNullPadding
    end,
    MeaningfullSize + PaddingSize.    

pixel_position(Image, {X, Y}) -> Image#image.row_size * Y + X * ?BYTES_PER_PIXEL.

get_pixel(Image, Point) ->
    Address = pixel_position(Image, Point),
    PixelData = binary_part(Image#image.contents, Address, ?BYTES_PER_PIXEL),
    <<Blue:8,Green:8,Red:8>> = PixelData,
    {Red, Green, Blue}.

change_pixel(Image, Point, {R, G, B}) ->
    Address = pixel_position(Image, Point),
    Prev = binary_part(Image#image.contents, 0, Address),
    SizeAfter = size(Image#image.contents)-Address-?BYTES_PER_PIXEL,
    After = binary_part(Image#image.contents, Address+?BYTES_PER_PIXEL, SizeAfter),
    NewPixel = <<B,G,R>>,
    Image#image{contents = <<Prev/binary,NewPixel/binary,After/binary>>}.

% hide_msg(Image, {X, Y}, Color, Msg) ->
%     Width = Image#image.width,
%     Height = Image#image.height,
%     <<First2Bits:2, Rest/bitstring>> = Msg,
%     {R, G, B} = get_pixel(Image, {X, Y}),
%     if Y < Height ->
%             if X < Width -> 
%                 case Color of
%                     0 -> 
%                         <<Begin:6, Last2Bits:2>> = <<R>>,
%                         New_color = Begin * 4 + First2Bits,
%                         NewImage = change_pixel(Image, {X, Y}, {New_color, G, B}),
%                         hide_msg(NewImage, {X, Y}, 1, Rest);
%                     1 -> 
%                         <<Begin:6, Last2Bits:2>> = <<G>>,
%                         New_color = Begin * 4 + First2Bits,
%                         NewImage = change_pixel(Image, {X, Y}, {R, New_color, B}),
%                         hide_msg(NewImage, {X, Y}, 2, Rest);
%                     2 -> 
%                         <<Begin:6, Last2Bits:2>> = <<B>>,
%                         New_color = Begin * 4 + First2Bits,
%                         NewImage = change_pixel(Image, {X, Y}, {R, G, New_color}),
%                         hide_msg(NewImage, {X+1, Y}, 0, Rest)
%                 end;
%             true ->
%                 hide_msg(Image, {0, Y+1}, Color, Msg)
%             end;
%     true ->
%         Image
%     end.

hide_msg(Image, {X, Y}, <<>>) ->
    Image;

hide_msg(Image, {X, Y}, Msg) ->
    Width = Image#image.width,
    Height = Image#image.height,
    <<First3Bits:3, Second3Bits:3, Last2Bits:2, Rest/bitstring>> = Msg,
    {R, G, B} = get_pixel(Image, {X, Y}),
    if Y < Height ->
            <<BeginR:5, LastR3Bits:3>> = <<R>>,
            New_r_color = BeginR * 8 + First3Bits,
            <<BeginG:5, LastG3Bits:3>> = <<G>>,
            New_g_color = BeginG * 8 + Second3Bits,
            <<BeginB:6, LastB2Bits:2>> = <<B>>,
            New_b_color = BeginB * 4 + Last2Bits,
            NewImage = change_pixel(Image, {X, Y}, {New_r_color, New_g_color, New_b_color}),

            if (X + 1) < Width ->
                New_X = X + 1,
                New_Y = Y;
            true ->
                New_X = 0,
                New_Y = Y+1
            end,
            hide_msg(NewImage, {New_X, New_Y}, Rest);
    true ->
        Image
    end.

extract_msg(Image) ->
    extract_char(Image, {0, 0}, 1, []).

extract_char(Image, {X, Y}, 0, Msg) ->
    Msg;

extract_char(Image, {X, Y}, Amount, Msg) ->
    Width = Image#image.width,
    Height = Image#image.height,
    if Y < Height ->
            {R, G, B} = get_pixel(Image, {X, Y}),
            <<BeginR:5, LastR3Bits:3>> = <<R>>,
            <<BeginG:5, LastG3Bits:3>> = <<G>>,
            <<BeginB:6, LastB2Bits:2>> = <<B>>,
            Char = LastR3Bits * 32 + LastG3Bits * 4 + LastB2Bits,
            case length(Msg) of
                0 ->
                    LastElem = 0;
                _ ->
                    LastElem = last(Msg)
            end,

            if (X+1) < Width -> 
                New_X = X+1,
                New_Y = Y;
            true ->
                New_X = 0,
                New_Y = Y+1
            end,

            if ((LastElem == 216) and (Char == 157)) ->
                New_Msg = lists:reverse(tl(lists:reverse(Msg))),
                extract_char(Image, {X, Y}, 0, New_Msg);
            true ->
                New_Msg = Msg ++ [Char],
                extract_char(Image, {New_X, New_Y}, Amount, New_Msg)
            end;
    true ->
        [-1]
    end.

cut_msg(Msg, Len) ->
    Substr = binary_part(Msg, {0, Len}),
    case unicode:characters_to_list(Substr) of
        {incomplete, _, _} ->
            if Len == 1->
                Tmp_msg = -1;
            true ->
                Tmp_msg = binary_part(Msg, {0, Len-1})
            end;
        _ ->
            Tmp_msg = Substr
    end,
    case Tmp_msg of
        -1 ->
            New_Msg = -1;
        _ ->
            New_Msg = <<Tmp_msg/binary, 216, 157>>
    end.

% Entry point for testing
main([FileName | _RestArguments]) ->
    {ok, Image} = load(FileName),
    io:fwrite("Image '~s' width: ~p, height: ~p~nRest arguments: ~p~n", [
        FileName,
        Image#image.width,
        Image#image.height,
        _RestArguments
    ]),

    [Cmd| Rest] = _RestArguments,

    case Cmd of
        "write" ->
            Txt = hd(Rest),
            OutFileName = tl(Rest),
            Msg = unicode:characters_to_binary(Txt),
            ImageSize = Image#image.width * Image#image.height,
            Len = min(ImageSize-2, bit_size(Msg) div 8),
            if Len =< 0 ->
                io:fwrite("Message is too large~n");
            true -> 
                New_Msg = cut_msg(Msg, Len),
                case New_Msg of
                    -1 ->
                        io:fwrite("Message is too large~n");
                    _ ->
                        io:fwrite("Current msg; ~ts~n", [New_Msg]),
                        NewImage = hide_msg(Image, {0, 0}, New_Msg),
                        save(OutFileName, NewImage)
                end
            end;
        "extract" ->
            Extracted_Msg = extract_msg(Image),
            case hd(Extracted_Msg) of
                -1 ->
                    io:fwrite("Nothing recorded~n");
                _ ->
                    io:fwrite("Extracted message: ~ts~n",[[list_to_binary(Extracted_Msg)]])
            end;
        _ ->
            io:fwrite("Bad argument~n")
    end.
